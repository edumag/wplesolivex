#!/bin/bash

DB=$1
DB_local=local.sql

dominio_servidor=lesolivex\.com
dominio_local=lesolivex\.ddev\.site

# sed 's/utf8mb4_0900_ai_ci/utf8mb4_general_ci/' "$1" > "$DB_local"

sed "s/$dominio_servidor/$dominio_local/g" "$1" > "$DB_local"

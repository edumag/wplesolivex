<?php

add_action( 'init', function () {
  
	$username      = defined("ADMIN_LESOLIVEX") ? ADMIN_LESOLIVEX : null;
	$email_address = defined("MAIL_LESOLIVEX") ? MAIL_LESOLIVEX : null;
  $password      = defined("PASS_LESOLIVEX") ? PASS_LESOLIVEX : null;

	if ( ! username_exists( $username ) ) {
    error_log("Creamos usuario admin [".$username."]");
		$user_id = wp_create_user( $username, $password, $email_address );
		$user = new WP_User( $user_id );
		$user->set_role( 'administrator' );
  }
	
} );

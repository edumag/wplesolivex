<?php
/*
Plugin Name: wplesolivex
Plugin URI: https://lesolivex.com
Description: Develop tools
Version: 1.0
Author: Eduard Magrané
Author URI: https://lesolivex.com
Text Domain: wplesolivex
*/

if ( file_exists(__DIR__ . '/config.php') ) {
  require_once(__DIR__ . '/config.php');
}

require_once(__DIR__ . '/admin_user.php');

/**
TRUNCATE `wp_commentmeta`;
TRUNCATE `wp_comments`;
*/

